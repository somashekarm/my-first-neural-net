My_first_neural_network

This is the code for the "My first Neural Network"

## Overview

This is a simple single layer feedforward neural network (perceptron). We use binary digits as our inputs and expect binary digits as our outputs. We'll use backpropagation via gradient descent to train our network and make our prediction as accurate as possible.

## Dependencies
numpy

## Python version
Python 3.5.2


## Usage

Run python  neuralnet.py in terminal to see it train, then predict.

## Challenge

The challenge is to create a 3 layer feed forward neural network using only numpy as your dependency.
Backpropagation usually involves recursively taking derivatives, but in our 1 layer  there was no recursion so was a trivial case of backpropagation.



## Credits
The credits for this code go to Milo Harper.
